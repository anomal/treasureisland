/**
 * Learn more about createBottomTabNavigator:
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */

import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import ForecastScreen from '../screens/ForecastScreen';
import TabMapScreen from '../screens/TabMapScreen';
import { BottomTabParamList, ForecastParamList, TabMapParamList } from '../types';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
    const colorScheme = useColorScheme();

    return (
        <BottomTab.Navigator initialRouteName="TabMap" tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
            <BottomTab.Screen
                name="TabMap"
                component={TabMapNavigator}
                options={{
                    tabBarLabel: 'Nearby Caches',
                    tabBarIcon: ({ color }) => <TabBarIcon name="ios-map" color={color} />
                }}
            />
            <BottomTab.Screen
                name="TabForecast"
                component={ForecastNavigator}
                options={{
                    tabBarLabel: 'Forecast',
                    tabBarIcon: ({ color }) => <TabBarIcon name="partly-sunny" color={color} />
                }}
            />
        </BottomTab.Navigator>
    );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: React.ComponentProps<typeof Ionicons>['name']; color: string }) {
    return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabMapStack = createStackNavigator<TabMapParamList>();

function TabMapNavigator() {
    return (
        <TabMapStack.Navigator>
            <TabMapStack.Screen
                name="TabMapScreen"
                component={TabMapScreen}
                options={{ headerTitle: 'Nearby Caches' }}
            />
        </TabMapStack.Navigator>
    );
}

const ForecastStack = createStackNavigator<ForecastParamList>();

function ForecastNavigator() {
    return (
        <ForecastStack.Navigator>
            <ForecastStack.Screen
                name="ForecastScreen"
                component={ForecastScreen}
                options={{ headerTitle: 'Forecast' }}
            />
        </ForecastStack.Navigator>
    );
}
