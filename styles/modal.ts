import { StyleSheet } from 'react-native';

export const modalStyles = StyleSheet.create({
    modalWrapper: {
        padding: 10,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        marginTop: 100,
        paddingBottom: 60,
        flexGrow: 1,
        elevation: 10,
        shadowColor: '#000',
        shadowOpacity: 0.3
    },
    modalHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15
    },
    modalHeaderText: {
        fontSize: 22
    }
});
