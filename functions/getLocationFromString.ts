import { LatLng } from 'react-native-maps';

export default function getCoordinatesFromString(locationString: `${number}|${number}`): LatLng {
    const coordsResult = { latitude: 0, longitude: 0 };
    const coords = locationString.split('|');
    if (coords && coords.length === 2) {
        coordsResult.latitude = parseFloat(coords[0]);
        coordsResult.longitude = parseFloat(coords[1]);
    }
    return coordsResult;
}
