export default function pluralWording(numberOfItems: number, label: string, displayValue = true): string {
    return displayValue
        ? numberOfItems === 1
            ? `${numberOfItems} ${label}`
            : `${numberOfItems} ${label}s`
        : numberOfItems === 1
        ? label
        : `${label}s`;
}
