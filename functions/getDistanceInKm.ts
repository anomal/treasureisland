import { getDistance } from 'geolib';
import { LatLng } from 'react-native-maps';

export default function getDistanceInKm(startLocation: LatLng, endLocation: LatLng) {
    if (!startLocation || !endLocation) {
        return '---';
    }

    let distance = getDistance(startLocation, endLocation);
    distance && (distance /= 1000);
    return `${distance} km`;
}
