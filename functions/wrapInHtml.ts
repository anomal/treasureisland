export default function wrapInHtml(content: string): string {
    return `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body>${content}</body></html>`;
}
