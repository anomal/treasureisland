function isArray(a: any) {
    return Array.isArray(a);
}

function isObject(o: any) {
    return o === Object(o) && !isArray(o) && typeof o !== 'function';
}

function toCamel(s: string) {
    return s.replace(/([-_][a-z])/gi, ($1: string) => {
        return $1.toUpperCase().replace('-', '').replace('_', '');
    });
}

export default function convertSnakeCaseToCamelCase(object: any) {
    if (isObject(object)) {
        const tempObject: any = {};

        Object.keys(object).forEach((k) => {
            tempObject[toCamel(k)] = convertSnakeCaseToCamelCase(object[k]);
        });

        return tempObject;
    } else if (isArray(object)) {
        return object.map((i: string) => {
            return convertSnakeCaseToCamelCase(i);
        });
    }

    return object;
}
