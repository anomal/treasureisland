import { useEffect, useState } from 'react';

export default function useDebounce(value: any, delay = 1000) {
    const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(() => {
        const handler = setTimeout(() => {
            setDebouncedValue(value);
        }, delay);

        // clear the timeout if the value or delay changes - this is the trick for the debouncer to work
        return () => {
            clearTimeout(handler);
        };
    }, [value, delay]);

    return debouncedValue;
}
