/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

export type RootStackParamList = {
    Root: undefined;
    NotFound: undefined;
};

export type BottomTabParamList = {
    TabMap: undefined;
    TabForecast: undefined;
};

export type ForecastParamList = {
    ForecastScreen: undefined;
};

export type TabMapParamList = {
    TabMapScreen: undefined;
};
