interface IWeatherAlert {
    senderName: string;
    event: string;
    start: number;
    end: number;
    description: string;
}

interface ITemperatureBasic {
    day: number;
    night: number;
    eve: number;
    morn: number;
}

interface ITemperature extends ITemperatureBasic {
    min: number;
    max: number;
}

interface IWeatherConditions {
    id: number;
    main: string;
    description: string;
    icon: string;
}

interface IWeatherBasic {
    dt: number;
    pressure: number;
    humidity: number;
    dewPoint: number;
    uvi: number;
    clouds: number;
    visibility: number;
    windSpeed: number;
    windDeg: number;
    weather: IWeatherConditions[];
}

interface ICurrentWeather extends IWeatherBasic {
    sunrise: number;
    sunset: number;
    temp: number;
    feelsLike: number;
    rain: {
        '1h': number;
    };
}

interface IWeatherMinutely {
    dt: number;
    precipitation: number;
}

interface IWeatherHourly extends IWeatherBasic {
    windGust: number;
    pop: number;
    temp: number;
    feelsLike: number;
}

interface IWeatherDaily extends IWeatherBasic {
    sunrise: number;
    sunset: number;
    moonrise: number;
    moonset: number;
    moonPhase: number;
    temp: ITemperature;
    feelsLike: ITemperatureBasic;
    pop: number;
    rain: number;
}

export interface IWeatherResponse {
    lat: number;
    lon: number;
    timezone: string;
    timezoneOffset: number;
    current: ICurrentWeather;
    minutely: IWeatherMinutely[];
    hourly: IWeatherHourly[];
    daily: IWeatherDaily[];
    alerts: IWeatherAlert[];
}

// it is actually not that simple, as it contains loads of data, but the data from another call is more usable. This one is for getting the city name only
// Tried Google Geocoding API for this, but it's not free.
export interface IWeatherResponseSimple {
    name: string;
}
