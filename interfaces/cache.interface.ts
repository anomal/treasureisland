export interface ICache {
    code: string;
    name: string;
    location: `${number}|${number}`;
    status: string;
    type: string;
}

export interface ICacheExtended extends ICache {
    founds: number;
    notfounds: number;
    watchers: number;
    size2: string;
    difficulty: number;
    terrain: number;
    rating: number;
    ratingVotes: number;
    recommendations: number;
    description: string;
    hint2: string;
    previewImage: ICacheImage;
}

export interface ICacheImage {
    uuid: string;
    url: string;
    thumbUrl: string;
    caption: string;
    uniqueCaption: string;
    isSpoiler: boolean;
}

export interface ICacheCodesResponse {
    results: string[];
}

export interface ICacheDetailsResponse {
    [key: string]: ICache;
}
