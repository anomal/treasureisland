import { Feather } from '@expo/vector-icons';
import Slider from '@react-native-community/slider';
import * as Location from 'expo-location';
import React, { useEffect, useRef, useState } from 'react';
import { Pressable, StyleSheet } from 'react-native';
import MapView, { Callout, Marker, PROVIDER_GOOGLE, Region } from 'react-native-maps';
import { OkapiApi } from '../api/okapi.api';
import CacheDetails from '../components/CacheDetails';
import CacheList from '../components/CacheList';
import { Text, View } from '../components/Themed';
import { tintColorLight } from '../constants/Colors';
import getDistanceInKm from '../functions/getDistanceInKm';
import getCoordinatesFromString from '../functions/getLocationFromString';
import useDebounce from '../hooks/useDebounce';
import { ICache, ICacheDetailsResponse } from '../interfaces/cache.interface';

const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = 0.0421;
const _okapiApi = new OkapiApi();

export default function TabMapScreen() {
    const [region, setRegion] = useState<Region>(null);
    const [userLocation, setUserLocation] = useState<Region>(null);
    const [errorMsg, setErrorMsg] = useState('');
    const [nearbyCaches, setNearbyCaches] = useState({} as ICacheDetailsResponse);
    const [selectedCacheCallout, setSelectedCacheCallout] = useState<ICache>(null);
    const [selectedCache, setSelectedCache] = useState<ICache>(null);
    const [radius, setRadius] = useState(10);
    const debouncedRegion = useDebounce(region);
    const markerRefs = useRef([]);
    let mapView: MapView;

    const _positionUserInView = () => {
        if (mapView && userLocation) {
            mapView.animateToRegion(userLocation);
        }
    };

    const handleCacheSelectForCallout = (cache: ICache) => {
        setSelectedCacheCallout(cache);
    };

    const openCacheDetails = (cache: ICache) => {
        setSelectedCache(cache);
    };

    const getDistanceToCache = (cache: ICache) => {
        return getDistanceInKm(getCoordinatesFromString(cache.location), userLocation);
    };

    // fetch user location on screen load
    useEffect(() => {
        (async () => {
            const { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
                return;
            }

            const location = await Location.getCurrentPositionAsync({}).catch((e) => {
                setErrorMsg(e);
                return;
            });

            if (location && 'coords' in location) {
                const region = {
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                    latitude: location.coords.latitude || 0,
                    longitude: location.coords.longitude || 0
                };
                setRegion(region);
                setUserLocation(region);
            }
        })();
    }, []);

    // fetch nearby cache on region change - debounced
    useEffect(() => {
        (async () => {
            if (
                !debouncedRegion ||
                !debouncedRegion.hasOwnProperty('latitude') ||
                !debouncedRegion.hasOwnProperty('longitude')
            ) {
                return;
            }

            const cacheCodes = await _okapiApi
                .fetchNearestCaches(debouncedRegion.latitude, debouncedRegion.longitude, radius || 10)
                .catch((e) => setErrorMsg(e));

            if (!cacheCodes || !cacheCodes.results) {
                return;
            }

            await _okapiApi
                .fetchCacheDetailsByCode(cacheCodes.results)
                .then((response) => setNearbyCaches(response))
                .catch((e) => setErrorMsg(e));
        })();
    }, [debouncedRegion, radius]);

    useEffect(() => {
        if (!selectedCacheCallout) {
            return;
        }
        const markerToNavigateTo = markerRefs.current.find((el) => el.props.title === selectedCacheCallout.name);
        if (!markerToNavigateTo) {
            return;
        }
        mapView.animateToCoordinate(markerToNavigateTo.props.coordinate);
        markerToNavigateTo.showCallout();
    }, [selectedCacheCallout]);

    return (
        <View style={styles.container}>
            {errorMsg.length > 0 && <Text>{errorMsg}</Text>}
            {!region && <Text>Loading</Text>}
            {region && (
                <View style={styles.mapWrapper}>
                    <MapView
                        ref={(map) => {
                            mapView = map;
                        }}
                        style={styles.map}
                        initialRegion={region}
                        onRegionChange={setRegion}
                        showsUserLocation={true}
                        provider={PROVIDER_GOOGLE}>
                        {nearbyCaches &&
                            Object.values(nearbyCaches).map((cache, index) => (
                                <Marker
                                    ref={(el) => (markerRefs.current[index] = el)}
                                    key={cache.location}
                                    title={cache.name}
                                    coordinate={getCoordinatesFromString(cache.location)}
                                    pinColor={cache.status === 'Available' ? 'green' : 'red'}>
                                    <Callout onPress={() => openCacheDetails(cache)}>
                                        <Text style={styles.calloutTitle}>{cache.name}</Text>
                                        <Text>{getDistanceToCache(cache)}</Text>
                                        <Text>{cache.status}</Text>
                                    </Callout>
                                </Marker>
                            ))}
                    </MapView>

                    <View style={[styles.mapToolbar, styles.mapToolbarTop]}>
                        <Pressable onPress={_positionUserInView} style={styles.mapToolbarButton}>
                            <Feather name="crosshair" size={40} color={tintColorLight} />
                        </Pressable>
                        <CacheList
                            caches={nearbyCaches}
                            selectCache={handleCacheSelectForCallout}
                            userLocation={userLocation}></CacheList>
                    </View>

                    <View style={[styles.mapToolbar, styles.mapToolbarBottom]}>
                        <Slider
                            style={styles.mapToolbarSlider}
                            minimumValue={1}
                            maximumValue={10}
                            step={1}
                            value={radius}
                            onSlidingComplete={setRadius}
                        />
                        <Text style={styles.mapToolbarSliderLabel}>Radius: {radius}km</Text>
                    </View>
                </View>
            )}
            <CacheDetails cache={selectedCache} userLocation={userLocation} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    mapWrapper: {
        flexGrow: 1,
        width: '100%'
    },
    map: {
        width: '100%',
        flexGrow: 1
    },
    mapToolbarTop: {
        top: 0,
        right: 0,
        justifyContent: 'flex-end'
    },
    mapToolbarBottom: {
        bottom: 0,
        justifyContent: 'space-between',
        width: '100%',
        flexDirection: 'row'
    },
    mapToolbar: {
        position: 'absolute',
        backgroundColor: 'transparent',
        alignItems: 'center',
        padding: 5
    },
    mapToolbarSlider: {
        flexGrow: 1,
        marginRight: 10,
        backgroundColor: 'transparent'
    },
    mapToolbarSliderLabel: {
        alignSelf: 'center'
    },
    mapToolbarButton: {
        padding: 5,
        backgroundColor: 'white',
        borderRadius: 100,
        marginBottom: 10
    },
    calloutTitle: {
        fontWeight: 'bold'
    }
});
