import { Feather } from '@expo/vector-icons';
import * as Location from 'expo-location';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { Image, ScrollView, StyleSheet } from 'react-native';
import { LatLng } from 'react-native-maps';
import { OpenWeatherApi } from '../api/open-weather.api';
import { Text, View } from '../components/Themed';
import convertSnakeCaseToCamelCase from '../functions/convertSnakeCaseToCamelCase';
import { IWeatherResponse } from '../interfaces/weather.interface';

const openWeatherApi = new OpenWeatherApi();

export default function ForecastScreen() {
    const [userLocation, setUserLocation] = useState<LatLng>(null);
    const [errorMsg, setErrorMsg] = useState('');
    const [weather, setWeather] = useState<IWeatherResponse>(null);
    const [cityName, setCityName] = useState('');

    const getIconUrl = (iconName: string): string => {
        return `http://openweathermap.org/img/wn/${iconName}@2x.png`;
    };

    const getTime = (timestamp: number): string => {
        if (!timestamp) {
            return '---';
        }
        const date = new Date(timestamp * 1000);
        const timeString = date.toLocaleTimeString('pl-PL');
        const timeArray = timeString.split(':');
        return `${timeArray[0]}:${timeArray[1]}`;
    };

    const getDayOfWeek = (timestamp: number): string => {
        if (!timestamp) {
            return '---';
        }
        const date = new Date(timestamp * 1000);
        switch (date.getUTCDay()) {
            case 1:
                return 'Monday';
            case 2:
                return 'Tuesday';
            case 3:
                return 'Wednesday';
            case 4:
                return 'Thursday';
            case 5:
                return 'Friday';
            case 6:
                return 'Saturday';
            case 0:
                return 'Sunday';
        }
    };

    const formatTemperature = (temperature: number): string => {
        if (!temperature) {
            return '---';
        }
        return `${temperature.toFixed(0)}°C`;
    };

    // fetch user location on screen load
    useEffect(() => {
        (async () => {
            const { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
                return;
            }

            const location = await Location.getCurrentPositionAsync({}).catch((e) => {
                setErrorMsg(e);
                return;
            });

            if (location && 'coords' in location) {
                const region = {
                    latitude: location.coords.latitude || 0,
                    longitude: location.coords.longitude || 0
                };
                setUserLocation(region);
            }
        })();
    }, []);

    useEffect(() => {
        (async () => {
            if (
                !userLocation ||
                !userLocation.hasOwnProperty('latitude') ||
                !userLocation.hasOwnProperty('longitude')
            ) {
                return;
            }

            const weather = await openWeatherApi.fetchWeatherForecast(userLocation.latitude, userLocation.longitude);
            const { name } = await openWeatherApi.fetchCurrentCityName(userLocation.latitude, userLocation.longitude);
            weather && setWeather(convertSnakeCaseToCamelCase(weather));
            name && setCityName(name);
        })();
    }, [userLocation]);

    return (
        <View style={styles.container}>
            {errorMsg.length > 0 && <Text>{errorMsg}</Text>}
            {(!weather || !cityName) && <Text>Loading</Text>}
            {weather && (
                <ScrollView>
                    <View style={styles.currentWeatherContainer}>
                        <Image style={styles.icon} source={{ uri: getIconUrl(weather.current?.weather[0]?.icon) }} />
                        <Text style={styles.title}>{cityName}</Text>
                        <Text style={styles.subTitle}>{weather.current?.weather[0]?.description}</Text>
                        <Text style={styles.currentTemperature}>{formatTemperature(weather.current?.temp)}</Text>
                        <Text>(feels like {formatTemperature(weather.current?.feelsLike)} tho)</Text>
                    </View>

                    <ScrollView horizontal={true} style={styles.hourlyContainer}>
                        {weather.hourly.map((el) => (
                            <View key={el.dt} style={styles.hourlyItem}>
                                <Text>{getTime(el.dt)}</Text>
                                <Image style={styles.iconSmall} source={{ uri: getIconUrl(el.weather[0]?.icon) }} />
                                <Text>{formatTemperature(el.temp)}</Text>
                            </View>
                        ))}
                    </ScrollView>

                    <View style={styles.dailyContainer}>
                        {weather.daily.map((el) => (
                            <View key={el.dt} style={styles.dailyItem}>
                                <Text style={styles.dailyItemName}>{getDayOfWeek(el.dt)}</Text>
                                <View style={styles.dailyItemDetailsHorizontal}>
                                    <Image style={styles.iconTiny} source={{ uri: getIconUrl(el.weather[0]?.icon) }} />
                                    <Text>{formatTemperature(el.temp.day)}</Text>
                                </View>
                                <View style={styles.dailyItemDetails}>
                                    <Feather name="arrow-up" size={15} color="black" />
                                    <Text style={styles.dailyItemText}>{formatTemperature(el.temp.max)}</Text>
                                </View>
                                <View style={styles.dailyItemDetails}>
                                    <Feather name="arrow-down" size={15} color="black" />
                                    <Text style={styles.dailyItemText}>{formatTemperature(el.temp.min)}</Text>
                                </View>
                                <View style={styles.dailyItemDetails}>
                                    <Feather name="sunrise" size={15} color="black" />
                                    <Text style={styles.dailyItemText}>{getTime(el.sunrise)}</Text>
                                </View>
                                <View style={styles.dailyItemDetails}>
                                    <Feather name="sunset" size={15} color="black" />
                                    <Text style={styles.dailyItemText}>{getTime(el.sunset)}</Text>
                                </View>
                            </View>
                        ))}
                    </View>

                    <View style={styles.currentWeatherDetailsOuterWrapper}>
                        <View style={styles.currentWeatherDetailsContainer}>
                            <View style={styles.currentWeatherDetails}>
                                <Text style={styles.currentWeatherDetailsHeader}>Sunrise</Text>
                                <Text style={styles.currentWeatherDetailsInfo}>
                                    {getTime(weather.current?.sunrise)}
                                </Text>
                            </View>
                            <View style={styles.currentWeatherDetails}>
                                <Text style={styles.currentWeatherDetailsHeader}>Sunset</Text>
                                <Text style={styles.currentWeatherDetailsInfo}>{getTime(weather.current?.sunset)}</Text>
                            </View>
                        </View>
                        <View style={styles.currentWeatherDetailsContainer}>
                            <View style={styles.currentWeatherDetails}>
                                <Text style={styles.currentWeatherDetailsHeader}>Pressure</Text>
                                <Text style={styles.currentWeatherDetailsInfo}>{weather.current?.pressure} hPa</Text>
                            </View>
                            <View style={styles.currentWeatherDetails}>
                                <Text style={styles.currentWeatherDetailsHeader}>Wind speed</Text>
                                <Text style={styles.currentWeatherDetailsInfo}>{weather.current?.windSpeed} km/h</Text>
                            </View>
                        </View>
                        <View style={styles.currentWeatherDetailsContainer}>
                            <View style={styles.currentWeatherDetails}>
                                <Text style={styles.currentWeatherDetailsHeader}>Humidity</Text>
                                <Text style={styles.currentWeatherDetailsInfo}>{weather.current?.humidity}%</Text>
                            </View>
                            <View style={styles.currentWeatherDetails}>
                                <Text style={styles.currentWeatherDetailsHeader}>Rain</Text>
                                <Text style={styles.currentWeatherDetailsInfo}>
                                    {(weather.current?.rain && weather.current?.rain['1h']) || 0}mm
                                </Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    currentWeatherContainer: {
        alignItems: 'center'
    },
    currentTemperature: {
        fontSize: 40,
        marginTop: 20
    },
    currentWeatherDetailsOuterWrapper: {
        marginVertical: 20
    },
    currentWeatherDetailsContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between'
    },
    currentWeatherDetails: {
        alignItems: 'center',
        marginVertical: 5,
        width: '50%'
    },
    currentWeatherDetailsHeader: {
        fontWeight: 'bold'
    },
    currentWeatherDetailsInfo: {
        fontWeight: '300'
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold'
    },
    subTitle: {
        fontSize: 18
    },
    icon: {
        width: 100,
        height: 100
    },
    iconSmall: {
        width: 50,
        height: 50
    },
    iconTiny: {
        width: 35,
        height: 35
    },
    hourlyContainer: {
        marginTop: 20
    },
    hourlyItem: {
        marginHorizontal: 5,
        alignItems: 'center'
    },
    dailyContainer: {
        marginTop: 20,
        marginHorizontal: 10
    },
    dailyItemName: {
        width: 100
    },
    dailyItem: {
        marginVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    dailyItemDetailsHorizontal: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    dailyItemDetails: {
        alignItems: 'center'
    },
    dailyItemText: {
        // fontSize: 10
    }
});
