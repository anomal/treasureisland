import { AntDesign } from '@expo/vector-icons';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Modal, Pressable, ScrollView, StyleSheet } from 'react-native';
import { Region } from 'react-native-maps';
import { Rating } from 'react-native-ratings';
import { WebView } from 'react-native-webview';
import { OkapiApi } from '../api/okapi.api';
import convertSnakeCaseToCamelCase from '../functions/convertSnakeCaseToCamelCase';
import getDistanceInKm from '../functions/getDistanceInKm';
import getCoordinatesFromString from '../functions/getLocationFromString';
import pluralWording from '../functions/pluralWording';
import wrapInHtml from '../functions/wrapInHtml';
import { ICache, ICacheExtended } from '../interfaces/cache.interface';
import { modalStyles } from '../styles/modal';
import { Text, View } from './Themed';

interface ICacheDetailsProps {
    cache: ICache;
    userLocation: Region;
}

const _okapiApi = new OkapiApi();

export default function CacheDetails(props: ICacheDetailsProps) {
    const [modalVisible, setModalVisible] = useState(false);
    const [cache, setCache] = useState<ICacheExtended>(null);

    const getDistanceToCache = (cache: ICache) => {
        return getDistanceInKm(getCoordinatesFromString(cache.location), props.userLocation);
    };

    useEffect(() => {
        (async () => {
            if (props.cache) {
                setModalVisible(true);
            } else {
                return;
            }

            if (props.cache.hasOwnProperty('code')) {
                const cacheResp = await _okapiApi.fetchCacheExtendedDetails(props.cache.code);
                const cacheFinal = convertSnakeCaseToCamelCase(cacheResp);

                if (cacheFinal) {
                    setCache(cacheFinal);
                    return;
                }
            }

            setCache(null);
        })();
    }, [props.cache]);

    return (
        <View>
            <Modal animationType="slide" transparent={true} visible={modalVisible}>
                {cache ? (
                    <View style={modalStyles.modalWrapper}>
                        <View style={modalStyles.modalHeader}>
                            <Text style={modalStyles.modalHeaderText}>{props.cache.name}</Text>
                            <Pressable
                                onPress={() => {
                                    setModalVisible(false);
                                }}>
                                <AntDesign name="close" size={24} color="black" />
                            </Pressable>
                        </View>
                        <ScrollView>
                            <View style={[styles.detailSection, styles.detailSectionHorizontal]}>
                                <View>
                                    <Text style={styles.detailName}>Code</Text>
                                    <Text>{cache.code}</Text>
                                </View>
                                <View>
                                    <Text style={styles.detailName}>Distance</Text>
                                    <Text>{getDistanceToCache(cache)}</Text>
                                </View>
                                <View>
                                    <Text style={styles.detailName}>Location</Text>
                                    <Text>{cache.location}</Text>
                                </View>
                            </View>
                            <View style={[styles.detailSection, styles.detailSectionHorizontal]}>
                                <View>
                                    <Text style={styles.detailName}>Status</Text>
                                    <Text>{cache.status}</Text>
                                </View>
                                <View>
                                    <Text style={styles.detailName}>Type</Text>
                                    <Text>{cache.type}</Text>
                                </View>
                                <View>
                                    <Text style={styles.detailName}>Size</Text>
                                    <Text>{cache.size2}</Text>
                                </View>
                            </View>

                            <View style={styles.detailSection}>
                                <Text style={styles.detailName}>Rating</Text>
                                <Rating readonly={true} startingValue={cache.rating} style={styles.rating} />
                                <Text>(from {pluralWording(cache.ratingVotes, 'vote')})</Text>
                                <Text>{pluralWording(cache.recommendations, 'recommendation')}</Text>
                                <Text>{pluralWording(cache.watchers, 'watcher')}</Text>
                            </View>
                            <View style={styles.detailSection}>
                                <Text style={styles.detailName}>Difficulty</Text>
                                <Rating readonly={true} startingValue={cache.difficulty} style={styles.rating} />
                                <Text>
                                    {pluralWording(cache.founds, 'found')} (
                                    {pluralWording(cache.notfounds, 'not found')})
                                </Text>
                            </View>
                            <View style={styles.detailSection}>
                                <Text style={styles.detailName}>Terrain</Text>
                                <Rating readonly={true} startingValue={cache.terrain} style={styles.rating} />
                            </View>
                            <View style={styles.detailSection}>
                                <Text style={styles.detailName}>Description</Text>
                                {cache.description ? (
                                    <WebView
                                        originWhitelist={['*']}
                                        source={{ html: wrapInHtml(cache.description) }}
                                        style={styles.webView}
                                    />
                                ) : (
                                    <Text>---</Text>
                                )}
                            </View>
                        </ScrollView>
                    </View>
                ) : (
                    <ActivityIndicator />
                )}
            </Modal>
        </View>
    );
}

const styles = StyleSheet.create({
    detailSection: {
        marginBottom: 15
    },
    detailSectionHorizontal: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    detailName: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 5
    },
    rating: {
        alignSelf: 'flex-start',
        marginBottom: 5
    },
    webView: {
        width: '100%',
        height: 500
    }
});
