import { AntDesign, MaterialCommunityIcons } from '@expo/vector-icons';
import React, { useEffect, useState } from 'react';
import { FlatList, Modal, Pressable, StyleSheet } from 'react-native';
import { Region } from 'react-native-maps';
import { tintColorLight } from '../constants/Colors';
import getDistanceInKm from '../functions/getDistanceInKm';
import getCoordinatesFromString from '../functions/getLocationFromString';
import { ICache, ICacheDetailsResponse } from '../interfaces/cache.interface';
import { modalStyles } from '../styles/modal';
import { Text, View } from './Themed';

interface ICacheListProps {
    caches: ICacheDetailsResponse;
    selectCache: Function;
    userLocation: Region;
}

export default function CacheList(props: ICacheListProps) {
    const [modalVisible, setModalVisible] = useState(false);
    const [caches, setCaches] = useState(null);

    const getDistanceToCache = (cache: ICache) => {
        return getDistanceInKm(getCoordinatesFromString(cache.location), props.userLocation);
    };

    const onCacheSelect = (cache: ICache) => {
        setModalVisible(false);
        props.selectCache(cache);
    };

    const handleCacheListUpdate = () => {
        const cachesTemp: ICache[] = [];
        props.caches && Object.values(props.caches).forEach((cache) => cachesTemp.push(cache));
        setCaches(cachesTemp);
    };

    useEffect(() => {
        handleCacheListUpdate();
    }, [props.caches]);

    const CacheTemplate = ({ item }: { item: ICache }) => (
        <View>
            <Pressable onPress={() => onCacheSelect(item)} style={styles.cacheListItem}>
                <View style={styles.cacheListItemColumnLeft}>
                    <Text style={styles.cacheListItemName}>{item.name}</Text>
                    <Text>{item.code}</Text>
                </View>
                <View>
                    <Text style={styles.cacheListItemTextRight}>{getDistanceToCache(item)}</Text>
                    <Text style={styles.cacheListItemTextRight}>{item.status}</Text>
                </View>
            </Pressable>
        </View>
    );

    return (
        <View style={styles.componentWrapper}>
            <Pressable
                style={styles.cacheListButton}
                onPress={() => {
                    setModalVisible(true);
                }}>
                <MaterialCommunityIcons name="treasure-chest" size={40} color={tintColorLight} />
                <View style={styles.cacheListBadge}>
                    <Text style={styles.cacheListBadgeText}>{caches?.length || 0}</Text>
                </View>
            </Pressable>

            <Modal animationType="slide" transparent={true} visible={modalVisible}>
                <View style={modalStyles.modalWrapper}>
                    <View style={modalStyles.modalHeader}>
                        <Text style={modalStyles.modalHeaderText}>Nearby Caches</Text>
                        <Pressable
                            onPress={() => {
                                setModalVisible(false);
                            }}>
                            <AntDesign name="close" size={24} color="black" />
                        </Pressable>
                    </View>
                    {caches && (
                        <FlatList<ICache>
                            ListEmptyComponent={<Text>No nearby caches found.</Text>}
                            data={caches}
                            renderItem={CacheTemplate}
                            keyExtractor={(item) => item.code}
                            style={styles.cacheList}
                        />
                    )}
                </View>
            </Modal>
        </View>
    );
}

const styles = StyleSheet.create({
    componentWrapper: {
        backgroundColor: 'transparent'
    },
    cacheList: {
        marginBottom: 20
    },
    cacheListItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 5
    },
    cacheListItemColumnLeft: {
        flexGrow: 1
    },
    cacheListItemTextRight: {
        textAlign: 'right'
    },
    cacheListItemName: {
        fontSize: 16,
        fontWeight: '600'
    },
    // TODO: below is duplicate of mapToolbarButton from TabMapScreen.tsx - refactor this so it's not repeated
    cacheListButton: {
        padding: 5,
        backgroundColor: 'white',
        borderRadius: 100
    },
    cacheListBadge: {
        position: 'absolute',
        backgroundColor: 'red',
        borderRadius: 100,
        padding: 2,
        right: 0
    },
    cacheListBadgeText: {
        color: 'white',
        fontWeight: 'bold'
    }
});
