import { CONSUMER_KEY } from '@env';
import { ICacheCodesResponse, ICacheDetailsResponse, ICacheExtended } from '../interfaces/cache.interface';

const OKAPI_URL = 'https://opencaching.pl/okapi/services/';

enum SEVERITY_LEVELS {
    ANONYMOUS = 0,
    BASIC = 1,
    OAUTH_BASIC = 2,
    OAUTH_SECURE = 3
}

export class OkapiApi {
    private _getAuthenticationTokens(severity: SEVERITY_LEVELS) {
        switch (severity) {
            case SEVERITY_LEVELS.ANONYMOUS:
                return {};
            case SEVERITY_LEVELS.BASIC:
                return {
                    consumer_key: CONSUMER_KEY
                };
            case SEVERITY_LEVELS.OAUTH_BASIC:
            case SEVERITY_LEVELS.OAUTH_SECURE:
                // TODO implement oauth
                return {};
        }
    }

    async fetchNearestCaches(latitude: number, longitude: number, radius = 10): Promise<ICacheCodesResponse> {
        if (!latitude || !longitude) {
            return;
        }

        const body = {
            ...this._getAuthenticationTokens(SEVERITY_LEVELS.BASIC),
            center: `${latitude}|${longitude}`,
            radius: radius.toString()
        };

        const url = `${OKAPI_URL}caches/search/nearest?`;
        const params = new URLSearchParams(body);
        const response = await fetch(url + params);
        return response.json();
    }

    async fetchCacheDetailsByCode(cacheCodes: ICacheCodesResponse['results']): Promise<ICacheDetailsResponse> {
        if (!cacheCodes) {
            return;
        }

        const body = {
            ...this._getAuthenticationTokens(SEVERITY_LEVELS.BASIC),
            cache_codes: cacheCodes.join('|')
        };

        const url = `${OKAPI_URL}caches/geocaches?`;
        const params = new URLSearchParams(body);
        const response = await fetch(url + params);
        return response.json();
    }

    async fetchCacheExtendedDetails(cacheCode: string): Promise<ICacheExtended> {
        if (!cacheCode) {
            return;
        }

        const body = {
            ...this._getAuthenticationTokens(SEVERITY_LEVELS.BASIC),
            cache_code: cacheCode,
            fields:
                'code|name|description|hint2|location|type|status|founds|notfounds|watchers|size2|difficulty|terrain|rating|rating_votes|recommendations|preview_image'
        };

        const url = `${OKAPI_URL}caches/geocache?`;
        const params = new URLSearchParams(body);
        const response = await fetch(url + params);
        return response.json();
    }
}
