import { OPEN_WEATHER_KEY } from '@env';
import { IWeatherResponse, IWeatherResponseSimple } from '../interfaces/weather.interface';

const OPEN_WEATHER_URL = 'https://api.openweathermap.org/data/2.5/';

export class OpenWeatherApi {
    private _getAuthenticationTokens() {
        return {
            appid: OPEN_WEATHER_KEY
        };
    }

    async fetchWeatherForecast(latitude: number, longitude: number): Promise<IWeatherResponse> {
        if (!latitude || !longitude) {
            return;
        }

        const body = {
            ...this._getAuthenticationTokens(),
            lat: latitude.toString(),
            lon: longitude.toString(),
            units: 'metric'
        };

        const url = `${OPEN_WEATHER_URL}onecall?`;
        const params = new URLSearchParams(body);
        const response = await fetch(url + params);
        return response.json();
    }

    async fetchCurrentCityName(latitude: number, longitude: number): Promise<IWeatherResponseSimple> {
        if (!latitude || !longitude) {
            return;
        }

        const body = {
            ...this._getAuthenticationTokens(),
            lat: latitude.toString(),
            lon: longitude.toString(),
            units: 'metric'
        };

        const url = `${OPEN_WEATHER_URL}weather?`;
        const params = new URLSearchParams(body);
        const response = await fetch(url + params);
        return response.json();
    }
}
